import { Component, OnInit } from '@angular/core';
import { setFirstTemplatePass } from '@angular/core/src/render3/state';
import { stringify } from '@angular/compiler/src/util';

export interface ToDoItem {
  title: string;
  description?: string;
  done: boolean;
}

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  items: ToDoItem[] = [];

  title: string;
  description: string;

  get isDone() {
    return this.items.some(element => element.done);
  }

  ngOnInit() {
    this.loadSavedCards();
  }
  private loadSavedCards() {
    this.items = JSON.parse(localStorage.getItem('todoItems')) || [];
  }

  private saveItem() {
    localStorage.setItem('todoItems', JSON.stringify(this.items));
  }

  add() {
    this.items.push({
      title: this.title,
      description: this.description,
      done: false
    });
    this.title = undefined;
    this.description = undefined;
    this.saveItem();
  }

  delete(index: number) {
    this.items.splice(index, 1);
    this.saveItem();
  }

  complete(index: number) {
    this.items[index].done = true;
  }
}
