import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { ToDoItem } from '../app.component';

@Component({
  selector: 'app-todo-item',
  templateUrl: './todo-item.component.html',
  styleUrls: ['./todo-item.component.scss']
})
export class TodoItemComponent implements OnInit {

  @Input() item: ToDoItem;
  @Output() deleteAtIndex: EventEmitter<number> = new EventEmitter();
  @Output() completeAtIndex: EventEmitter<number> = new EventEmitter();

  deleteButton() {
    this.deleteAtIndex.emit();
  }

  completeButton() {
    this.completeAtIndex.emit();
  }

  ngOnInit() {
  }

}
